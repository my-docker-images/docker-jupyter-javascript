
REPO = gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-jupyter-javascript
TAG  = latest

all:
	docker buildx create --use --node new-builder
	# 2024-02-28: qemu: uncaught target signal 11 (Segmentation fault)
	#docker buildx build --push --platform "linux/amd64","linux/arm64" --tag "${REPO}:${TAG}" .
	docker buildx build --provenance=false --push --platform "linux/amd64" --tag "${REPO}:${TAG}" .

