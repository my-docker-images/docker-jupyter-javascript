#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

set -e

echo "$(hostname -I | cut -f2 -d' ') $HOSTNAME" >> /etc/hosts

cp -TRn /subjects/ /home/$NB_USER/work

fix-permissions "/home/$NB_USER"

export JUPYTER_TOKEN=${2}

. /usr/local/bin/start.sh $wrapper jupyter lab \
    --ServerApp.shutdown_no_activity_timeout=3600 \
    --MappingKernelManager.cull_idle_timeout=3600 \
    --MappingKernelManager.cull_interval=300 \
    --TerminalManager.cull_inactive_timeout=3600 \
    --TerminalManager.cull_interval=300

