
# docker-jupyter-javascript

## Why

This image is a base image for doing JavaScript with Jupyter Lab. 
It is based on [Minimal Jupyter Notebook Stack](https://github.com/jupyter/docker-stacks/tree/main/minimal-notebook).

## Changes

- node, npm and [IJavascript](https://github.com/n-riesco/ijavascript) added.
- passwordless sudo enable.
- wrapper_script for use with MyDocker.
- /subjects directory for files to be copied in user's home by wrapper_script.

## Details

- The exposed port is 8888
- The user folder is `/home/jovyan`
- if docker is installed on your computer, you can run (amd64 or arm64 architecture) this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/home/jovyan/work`, with:
  
  `docker run -p 8888:8888 -v "$(pwd):/home/jovyan/work"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-jupyter-javascript`

