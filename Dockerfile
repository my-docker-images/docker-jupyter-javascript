
FROM jupyter/minimal-notebook:latest

USER root

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update --yes && \
    apt-get install --yes nodejs npm && \
    apt-get autoremove && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/*

RUN npm install -g ijavascript

RUN ijsinstall

## Enable sudo
RUN echo "${NB_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/added-by-start-script

## add local files
COPY /root /

## Folder for subjects to be copied by wrapper in /home/jovyan/work directory
RUN mkdir -p /subjects/

ENTRYPOINT ["/bin/bash", "/usr/local/lib/wrapper_script.sh"]

